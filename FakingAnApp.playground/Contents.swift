
import Foundation
import PlaygroundSupport

PlaygroundPage.current.needsIndefiniteExecution = true

struct IPv4Address {
    /// The 4 different parts of the address, i.e. a.b.c.d
    let parts: [Int16]
}

extension IPv4Address {
    /// Attempts to parse an address from a string. Returns `nil` if failure occurred.
    init?(from str: String) {
        let splits = str.components(separatedBy: ".")

        guard splits.count == 4 else {
            return nil
        }

        self.parts = splits.flatMap() { Int16($0) }
    }
}

extension IPv4Address: CustomStringConvertible {
    var description: String {
        return parts.map(String.init).joined(separator: ".")
    }
}

/// A type which can asynchronously retrieve the IP address in of the current device.
protocol IPAddressGetter {
    /// Fetches the IP address of the current device. Being an async call, the result 
    /// is made available in the callback.
    func fetchIPAddress(completion: @escaping (IPv4Address) -> ())
}

/// An IPAddressGetter which fakes the returned data.
struct FakeIPAddressGetter: IPAddressGetter {
    /// The address returned by a call to `fetchIPAddress`.
    let address: IPv4Address

    func fetchIPAddress(completion: @escaping (IPv4Address) -> ()) {
        completion(address)
    }
}

/// An IPAddressGetter which doesn't fake the data, and actually gets the IP address.
struct RealIPAddressGetter: IPAddressGetter {
    func fetchIPAddress(completion: @escaping (IPv4Address) -> ()) {
        let url = URL(string: "https://ifconfig.co/json")!
        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            if let error = error {
                print("Error: \(error)")
                return
            }

            if let json = try? JSONSerialization.jsonObject(with: data!, options: []),
                let dict = json as? [String: Any],
                let strAddress = dict["ip"] as? String,
                let addr = IPv4Address(from: strAddress) {

                completion(addr)

            } else {
                print("Error")
            }
        }

        task.resume()
    }
}

let fakeAddress = IPv4Address(parts: [0, 0, 0, 0])
FakeIPAddressGetter(address: fakeAddress).fetchIPAddress() { addr in
    print("Fake Address : \(addr)")
}

RealIPAddressGetter().fetchIPAddress() { addr in
    print("Real Address : \(addr)")
}
